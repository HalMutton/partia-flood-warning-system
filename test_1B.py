from floodsystem.geo import *
from floodsystem.stationdata import build_station_list


def test_stations_by_distance():
    "testing if stations are sorted according to distance"
    stations = build_station_list()
    stations_by_dist = stations_by_distance(stations, (0,0))
    stations_list = [(station.name, distance) for (station, distance) in stations_by_dist]
    for i in range(1,len(stations)): 
        "See if the station before it has less distance"
        assert stations_list[i-1][1] <= stations_list[i][1]