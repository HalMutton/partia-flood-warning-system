from floodsystem import geo, stationdata

def test_1Ci():
    """Automated testing of stations_within_radius function"""
    l = geo.stations_within_radius(stationdata.build_station_list(), (52.2053, 0.1218), 10)
    output = [(i.name) for (i) in l]
    assert 'Cambridge Jesus Lock' in output
    
def test_1Cii():
    l = geo.stations_within_radius(stationdata.build_station_list(), (52.2053, 0.1218), 10)
    assert 'Penzance Tesco' not in l