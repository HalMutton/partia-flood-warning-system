from floodsystem import geo, flood, station, stationdata
from floodsystem.stationdata import *
from floodsystem.utils import *
def a(stations, tol):
    output = []
    for station in stations:
        if  MonitoringStation.relative_water_level(station)==None:
                pass
        elif MonitoringStation.relative_water_level(station) > tol:
                a= (station, MonitoringStation.relative_water_level(station))
                output.append(a)
        else:
                pass

                #output.append({station.name: station.relative_water_level(station)})
    stations_over_tol = sorted_by_key(output,1,reverse=True)
    return stations_over_tol[1]


def b(stations, N):
        stations_high_waterlevel = a(stations, 0)
        stations_high_waterlevel = stations_high_waterlevel[:N]
        return stations_high_waterlevel

def run():
    
    stations = stationdata.build_station_list()
    update_water_levels(stations)
    print (flood.flood_warning(stations))

    #stations = b(stations,10)
    #print (flood.flood_warning(stations))
if __name__ == "__main__":
    print("*** Task 2G: CUED Part IA Flood Warning System ***")

    run()