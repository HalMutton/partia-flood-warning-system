import pytest
import floodsystem
from floodsystem.flood import *
from floodsystem.stationdata import *


def test_stations_level_over_threshold():
    stations =build_station_list()
    update_water_levels(stations)
    for i in stations_level_over_threshold (stations,0.8):
        assert type(i)==tuple
        assert i[1]>=0.8

def test_stations_highest_rel_level():
    stations=build_station_list()
    update_water_levels(stations)
    stations_list=[]
    N=5
    for i in stations_highest_rel_level(stations,N):
        stations_list.append(i)
    assert len(stations_list)==N