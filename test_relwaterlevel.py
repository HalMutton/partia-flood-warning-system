import pytest
from floodsystem.station import MonitoringStation
from floodsystem.station import inconsistent_typical_range_stations
from floodsystem.stationdata import build_station_list
from floodsystem.stationdata import update_water_levels  

def test_relative_water_level():
    
    s_id = "test-s-id"
    m_id = "test-m-id"
    label = "some station"
    coord = (-2.0, 4.0)
    trange = (0, 1)
    river = "River X"
    town = "My Town"
    s = MonitoringStation(s_id, m_id, label, coord, trange, river, town)
    s.latest_level = 0
    
    assert MonitoringStation.relative_water_level(s) == None

    stations = update_water_levels(build_station_list())
    
    stations = stations[1]

    assert type(MonitoringStation.relative_water_level(stations)) == float
    
    