from floodsystem.stationdata import *
from floodsystem.flood import *

def run():
    station = build_station_list()
    stationupdated = update_water_levels(station)
    stations_over_tol= stations_level_over_threshold(stationupdated,0.8)
    for i in range (len(stations_over_tol)):
        print ("{},{}".format(stations_over_tol[i][0].name,stations_over_tol[i][1]))


if __name__ == "__main__":
    print("*** Task 2B: CUED Part IA Flood Warning System ***")
    run() 