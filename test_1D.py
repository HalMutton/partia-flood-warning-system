from floodsystem import geo, stationdata

def test_1Di():

    assert geo.rivers_with_station([stationdata.build_station_list()[0]]) == {'River Dikler': ['Bourton Dickler']}

#def test_1Dii():
    #assert 'Cambridge Jesus Lock' in geo.stations_by_river('River Cam')
