from floodsystem.geo import *
from floodsystem.stationdata import build_station_list

def run():
    shortest=[]
    furthest=[]
    p = (52.2053,0.1218)
    stations = build_station_list()
    stationssorted= stations_by_distance(stations,p)
    for i in range(10):
        shortest.append((stationssorted[i][0].name,stationssorted[i][0].town, stationssorted[i][1]))
        furthest.append((stationssorted[-10+i][0].name, stationssorted[-10+i][0].town, stationssorted[-10+i][1]))
    print (shortest)
    print (furthest)

if __name__ == "__main__":
    print("*** Task 1B: CUED Part IA Flood Warning System ***")
    run()