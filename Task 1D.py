from floodsystem.geo import *
from floodsystem.stationdata import *

def run():
    output = rivers_with_station(stationdata.build_station_list())
    sorted_output = sorted(output)
    
    print('\nNumber of rivers with a station is ', len(output), ". \n")
    print("First 10 rivers with stations: \n", sorted_output[:9])
    rivers = stations_by_river(stationdata.build_station_list())

    stations_by_river('River Aire')
    stations_by_river('River Cam')
    stations_by_river('River Thames')

if __name__ == "__main__":
    print("*** Task 1D: CUED Part IA Flood Warning System ***")
    run()