import matplotlib.pyplot as plt
import matplotlib.dates as dat
import numpy as np

def polyfit(dates, levels, p):
    #convert dates to float values#
    x = dat.date2num(dates)
    #subract smallest date number from all others to reduce roundoff errors#
    x = sorted(x, reverse = True)
    for datenum in x:
        datenum -= x[len(x)-1]
    x = sorted(x)
    #use polyfit to find polynomial approximation for water levels#
    p_coeff = np.polyfit(x, levels, p)
    poly = np.poly1d(p_coeff)
    
    plt.plot(x, levels, '.')
    x1 = np.linspace(x[0], x[-1], 30)
    plt.plot(x1, poly(x1))
    plt.show()
