from floodsystem.station import *
from floodsystem.utils import *

def stations_level_over_threshold(stations, tol):
    output = []
    for station in stations:
        if  MonitoringStation.relative_water_level(station)==None:
                pass
        elif MonitoringStation.relative_water_level(station) > tol:
                a= (station, MonitoringStation.relative_water_level(station))
                output.append(a)
        else:
                pass

                #output.append({station.name: station.relative_water_level(station)})
    stations_over_tol = sorted_by_key(output,1,reverse=True)
    return stations_over_tol
    
def takelevel(station_level):
        return station_level.keys()

def stations_highest_rel_level(stations, N):
        stations_high_waterlevel = stations_level_over_threshold(stations, 0)
        stations_high_waterlevel = stations_high_waterlevel[:N]
        return stations_high_waterlevel

def flood_warning(stations):
        output = {}
        for station in stations:
                if  MonitoringStation.relative_water_level(station)==None:
                        output[station.name] = 'Invalid data'
                elif MonitoringStation.relative_water_level(station) > 1.5:
                        output[station.name] = 'Severe'
                elif MonitoringStation.relative_water_level(station) > 0.9:
                        output[station.name] = 'High'
                elif MonitoringStation.relative_water_level(station) > 0.5:
                        output[station.name] = 'Moderate'
                else:
                        output[station.name] = 'Low'

        return output




