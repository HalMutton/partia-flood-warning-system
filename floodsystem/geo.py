# Copyright (C) 2018 Garth N. Wells
#
# SPDX-License-Identifier: MIT
"""This module contains a collection of functions related to
geographical data.

"""
from floodsystem import station
from floodsystem.utils import sorted_by_key  # noqa
from haversine import haversine
from floodsystem import stationdata
def stations_by_distance(stations, p):
    dist_list=[]
    for station in stations:
        distance = haversine(p, station.coord)
        abc = (station, distance)
        dist_list.append(abc)
    return sorted_by_key(dist_list, 1)

def stations_within_radius(stations, centre, r):
    """creates list of stations within a given radius"""
    p = []
    for station in stations:
        x = haversine(centre, station.coord)
        if x <= r:
            p.append(station)
    return p

def rivers_with_station(stations):

    """returns list of rivers with stations nearby"""

    
    p = [[s.river, s.name] for s in stations]
    rivers = {}
    for (r, s) in p:
        if r in rivers:
            rivers[r].append(s)
        else:
            rivers[r] = [s]
    return rivers
def stations_by_river(river):
    output = []
    for s in stationdata.build_station_list():
        if s.river == river:
            output.append(s.name)
    print (river)
    print (output)
#def stations_by_river(stations):
    #"""returns list of monitoring stations near a given river"""
    #rivers = rivers_with_station(stations)
    #output = {}
    #for r in rivers:
     #   ss = []
      #  for s in stations:
       #     if s.river is  r:
        #        ss.append(s)
        #output[r] = ss
    
   # return output 

def rivers_by_station_number(stations, N):
    riverdic= {}
    riverdic_list=[]
    for station in stations:
    
        if station.river not in riverdic:
            riverdic[station.river]=1
        else:
            riverdic[station.river]+=1
    for key in riverdic:
        if riverdic[key]>=N:        
            riverdic_list.append((key,riverdic[key]))
        else:
            pass
    return sorted_by_key(riverdic_list, 1, reverse=True)

