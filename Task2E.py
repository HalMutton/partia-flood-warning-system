
import datetime
from floodsystem.flood import stations_highest_rel_level
from floodsystem.datafetcher import fetch_measure_levels
from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.plot import plot_water_levels

def run():
    
    stations = build_station_list()
    
    stations = update_water_levels(stations)
    
    stations_highest = stations_highest_rel_level(stations, 5)
    
    dt = 10
    
    for station in stations_highest:
        
        dates, levels = fetch_measure_levels(station[0].measure_id, datetime.timedelta(dt))
        
        if len(dates) == 0:
            print("Not enough data available for {}".format(station[0].name))
        else:
            plot_water_levels(station[0], dates, levels)
            
if __name__ == "__main__":
    print("*** Task 2E: CUED Part IA Flood Warning System ***")
    run()


#def stations_highest_rel_level2(stations, N):
 #       reference = []
  #      for station in stations:
   #             reference.append(station.relative_water_level(station))
    #    reference = sorted(reference, reverse=True)
     #   output = []
      #  for station in stations:
       #         if station.relative_water_level(station) > reference[N]:
        #                output.append(station)
        #return output

#def run():
    #dates = [datetime(2019, 2, 26), datetime(2019, 2, 25), datetime(2019, 2, 24), datetime(2019, 2, 23), datetime(2019, 2, 22), datetime(2019, 2, 21), datetime(2019, 2, 20), datetime(2019, 2, 19), datetime(2019, 2, 18), datetime(2019, 2, 17)]
    #stations = stations_highest_rel_level2(stationdata.build_station_list(), 5)
    #for station in stations:
    #    print(plot.plot_water_levels(station, dates))