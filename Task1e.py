from floodsystem.geo import *
from floodsystem.stationdata import build_station_list


def run():
    stations = build_station_list()
    riverstation= rivers_by_station_number(stations,9)
    print (riverstation)
if __name__ == "__main__":
    print("*** Task 1E: CUED Part IA Flood Warning System ***")
    run()