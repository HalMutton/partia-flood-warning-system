from floodsystem.flood import *
from floodsystem.stationdata import *


def run ():
    stations = build_station_list()
    update_water_levels(stations)    
    stations_high = stations_highest_rel_level(stations, 10)
    for i in range(len(stations_high)):
        print("{}, {}".format(stations_high[i][0].name, stations_high[i][1]))
        
if __name__ == "__main__":
    print("*** Task 2C: CUED Part IA Flood Warning System ***")
    run()
    