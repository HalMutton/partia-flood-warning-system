from floodsystem.plot import plot_water_level_with_fit
from floodsystem.stationdata import build_station_list
from floodsystem.datafetcher import fetch_measure_levels
from floodsystem.flood import stations_highest_rel_level
from floodsystem.plot import plot_water_levels
from floodsystem.stationdata import update_water_levels
import datetime

def test_plot_water_levels():
    stations = build_station_list()
    
    stations = update_water_levels(stations)
    
    stations_highest = stations_highest_rel_level(stations, 5)
    
    dt = 2
    
    dates, levels = fetch_measure_levels(stations_highest[0][0].measure_id, datetime.timedelta(dt))
    plot_water_levels(stations_highest[0][0], dates, levels)


