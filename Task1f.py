from floodsystem.station import *
from floodsystem.stationdata import build_station_list
from floodsystem.utils import sorted_by_key

def run():
    stations = build_station_list()
    a= inconsistent_typical_range_stations(stations)
    print ("Stations with inconsistent data:{}".format(a))

if __name__ == "__main__":
    print("*** Task 1F: CUED Part IA Flood Warning System ***")
    run() 